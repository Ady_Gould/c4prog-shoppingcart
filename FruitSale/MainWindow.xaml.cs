﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;


namespace FruitSale
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private List<Product> prodList;
        private List<Product> shoppingCart;

        public MainWindow()
        {
            InitializeComponent();
            InitProducts();
            FillProdList();
            CreateShoppingCart();
        }

        private void InitProducts()
        {
            Product p;
            prodList = new List<Product>();

            p = new Product();
            p.Name = "Apple, Fuji";
            p.StockLevel = 100.0d;
            p.PricePerKg = 2.95d;
            prodList.Add(p);

            p = new Product();
            p.Name = "Banana, Lady Finger";
            p.StockLevel = 20.0d;
            p.PricePerKg = 18.60d;
            prodList.Add(p);

            p = new Product();
            p.Name = "Blueberries";
            p.StockLevel = 40.0d;
            p.PricePerKg = 12.35d;
            prodList.Add(p);

            p = new Product();
            p.Name = "Durian";
            p.StockLevel = 10.0d;
            p.PricePerKg = 45.80d;
            prodList.Add(p);
        } // end InitProducts


        void FillProdList()
        {

            // Add columns
            var gridView = new GridView();
            ProductsListView.View = gridView;

            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Product",
                DisplayMemberBinding = new Binding("Name")
            });
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Price/Kg",
                DisplayMemberBinding = new Binding("PricePerKg")
            });
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Stock",
                DisplayMemberBinding = new Binding("StockLevel")
            });

            ProductsListView.ItemsSource = prodList;

        }

                    private void CreateShoppingCart()
        {
            // Add columns
            var gridView = new GridView();
            ProductsListView.View = gridView;

            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Product",
                DisplayMemberBinding = new Binding("Name")
            });
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Price/Kg",
                DisplayMemberBinding = new Binding("PricePerKg")
            });
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Stock",
                DisplayMemberBinding = new Binding("StockLevel")
            });

            ProductsListView.ItemsSource = prodList;

        }


        private void AddToCartButton_Click(object sender, RoutedEventArgs e)
        {
            double qty = Convert.ToDouble(QuantityTextbox.Text);
            Product p = new Product();
            int item = -1;

            p = (Product)ProductsListView.SelectedItem;
            if (p.HasStock() && p.EnoughStock(qty))
            {
                p.StockLevel -= qty;
            }
            ProductsListView.Items.Refresh();


        }

    }
}
