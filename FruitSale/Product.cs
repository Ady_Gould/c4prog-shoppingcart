﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FruitSale
{
    class Product
    {
        private String productName = "UNKNOWN";
        private double stockLevel = 0;
        private double pricePerKg = 0;
        private double quantity = 0;


        public String Name
        {
            get { return productName; }
            set { productName = value; }
        }

        public double StockLevel
        {
            set { stockLevel = value; }
            get { return stockLevel; }
        }

        public double PricePerKg
        {
            get { return pricePerKg; }
            set { pricePerKg = value; }
        }

        public double Quanity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        public bool HasStock()
        {
            return stockLevel > 0;
        }

        public bool EnoughStock(double quantity)
        {
            return (stockLevel >= quantity);
        }

    }//end class
}
